import { Component, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'serviceWorkerTest';
  data = "";
  constructor(
    private http: HttpClient,
    private swUpdate: SwUpdate) {}

  ngOnInit(){
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {

          if(confirm("New version available. Load New Version?")) {

              window.location.reload();
          }
      });
    }
    this.getShedule().subscribe((data:any)=>{
      this.data = data.errMsg;
    })
    
    Notification.requestPermission().then(perm=>{
      if (perm == "granted") {
        console.log("Perm granted");
      }
      // console.log(`Perm : ${perm}`);
    })
    
    this.getWeeklyEvents().subscribe((data:any)=>{
      this.data = data.errMsg;
      console.log("events",data);
    })

    this.getYearGrades().subscribe((data:any)=>{
      this.data = data.errMsg;
      console.log("grades",data);
    })
  }      

  getShedule(){
    let post = {
      token: "eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkExMjhHQ00ifQ.D_vUMlA19MueFANznkGmVcxV81LoSpoBto67QAv6Y8Cmo3WRyXpyNpaf03OYLFwZrTXPerFNJ4ayESWZx3hyiKSz9kcppOFfJrSX-mEofATtHAk5x0ZBImyaDqwA68163Pn3ScHGL_ZdlR_KRF8CiQ4GwGpembJ5We6uTe1EXPNFzadZPgVtbKhsBdSZG7emPNYP4Snc8B3AlIS9AN3IJ1L15zRNi1yUc_sVI6nU_dibHNfALzIkYu_DjI9LS2awhcsp595ZLEPaBxxTdRLHVcZ4QAEeeoJ6Q6K9ZG5seVvq0m3b1dSSD-zbhxKuaSgYK4PheZ1pUukB8W4z0W4fFw.HMOYaEuoNY5-qOqw.aOuS9S9L1gh9xP6ECYY0N1VWTFK5XHM.9Zc0JCCup6AtF4BRGtPIqA"
    }
    return this.http.post("https://np777gmeqe.execute-api.eu-central-1.amazonaws.com/dev/getSchedule", post);
    
  }

  getWeeklyEvents(){
    let post = {
      token: "eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkExMjhHQ00ifQ.D_vUMlA19MueFANznkGmVcxV81LoSpoBto67QAv6Y8Cmo3WRyXpyNpaf03OYLFwZrTXPerFNJ4ayESWZx3hyiKSz9kcppOFfJrSX-mEofATtHAk5x0ZBImyaDqwA68163Pn3ScHGL_ZdlR_KRF8CiQ4GwGpembJ5We6uTe1EXPNFzadZPgVtbKhsBdSZG7emPNYP4Snc8B3AlIS9AN3IJ1L15zRNi1yUc_sVI6nU_dibHNfALzIkYu_DjI9LS2awhcsp595ZLEPaBxxTdRLHVcZ4QAEeeoJ6Q6K9ZG5seVvq0m3b1dSSD-zbhxKuaSgYK4PheZ1pUukB8W4z0W4fFw.HMOYaEuoNY5-qOqw.aOuS9S9L1gh9xP6ECYY0N1VWTFK5XHM.9Zc0JCCup6AtF4BRGtPIqA"
    }
    return this.http.post("https://np777gmeqe.execute-api.eu-central-1.amazonaws.com/dev/getWeeklyEvents", post);
  }

  getYearGrades(){
    let post = {
      token: "eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkExMjhHQ00ifQ.D_vUMlA19MueFANznkGmVcxV81LoSpoBto67QAv6Y8Cmo3WRyXpyNpaf03OYLFwZrTXPerFNJ4ayESWZx3hyiKSz9kcppOFfJrSX-mEofATtHAk5x0ZBImyaDqwA68163Pn3ScHGL_ZdlR_KRF8CiQ4GwGpembJ5We6uTe1EXPNFzadZPgVtbKhsBdSZG7emPNYP4Snc8B3AlIS9AN3IJ1L15zRNi1yUc_sVI6nU_dibHNfALzIkYu_DjI9LS2awhcsp595ZLEPaBxxTdRLHVcZ4QAEeeoJ6Q6K9ZG5seVvq0m3b1dSSD-zbhxKuaSgYK4PheZ1pUukB8W4z0W4fFw.HMOYaEuoNY5-qOqw.aOuS9S9L1gh9xP6ECYY0N1VWTFK5XHM.9Zc0JCCup6AtF4BRGtPIqA"
    }
    return this.http.post("https://np777gmeqe.execute-api.eu-central-1.amazonaws.com/dev/getYearGrades", post);
  }
}
